from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Sum
from django.urls import reverse
from django.utils import timezone

User = get_user_model()


class Post(models.Model):
    """
    Model for posts
    """

    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )

    title = models.CharField(max_length=255, verbose_name='Post title')
    body = models.TextField(verbose_name='Post body')
    slug = models.SlugField(max_length=125, unique=True)
    image = models.ImageField(upload_to='%Y/%m/%d/', null=True, blank=True)
    status = models.CharField(
        max_length=15, choices=STATUS_CHOICES, default='draft'
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    published = models.DateTimeField(default=timezone.now)

    # relations
    author = models.ForeignKey(
        'Author', on_delete=models.CASCADE, related_name='posts'
    )
    tags = models.ManyToManyField('Tag', related_name='posts', blank=True)

    class Meta:
        ordering = ('-published',)

    def __str__(self):
        return self.title

    def get_rating(self):
        return self.votes.aggregate(Sum('value'))

    def get_absolute_url(self):
        return reverse('blog:post_detail', args=[self.slug])


class Author(models.Model):

    first_name = models.CharField(max_length=60, verbose_name='First name')
    last_name = models.CharField(max_length=120, verbose_name='Last name')
    slug = models.SlugField(max_length=165, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_absolute_url(self):
        return reverse('blog:posts_by_author', args=[self.slug])


class Tag(models.Model):

    title = models.CharField(max_length=155)
    slug = models.SlugField(max_length=165, unique=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:posts_bay_tag', args=[self.slug])


#  Модель Votes создана на будущее, если реализовать в проекте регистрацию,
#  можно будет легко добавить возможность для пользователей оценивать посты


class Vote(models.Model):
    """
    Model for the formation of posts rating.
    Post can be up-voted or down-voted by user.
    """
    VOTE_CHOICES = (
        (1, 'Upvote!'),
        (-1, 'Downvote!')
    )

    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='votes'
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='votes'
    )
    value = models.SmallIntegerField(choices=VOTE_CHOICES)

    class Meta:
        unique_together = ['post', 'user']
