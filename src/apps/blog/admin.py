from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import (
    Post, Tag, Author, Vote
)


@admin.register(Post)
class PostAdmin(ModelAdmin):
    list_display = ('title', 'status', 'published')
    list_editable = ('status', )
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ['title', 'body']


@admin.register(Author)
class AuthorAdmin(ModelAdmin):
    list_display = ('first_name', 'last_name')
    prepopulated_fields = {'slug': ('first_name', 'last_name')}
    search_fields = ['first_name', 'last_name']


@admin.register(Tag)
class TagAdmin(ModelAdmin):
    list_display = ('title',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ['title']


@admin.register(Vote)
class VoteAdmin(ModelAdmin):
    list_display = ('post', 'user', 'value')
