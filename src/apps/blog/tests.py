from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse, resolve

from .models import (
    Post, Tag, Author, Vote
)
from .views import PostListView, TagListView, AuthorListView, PostDetailView


#  Models testing -----------------------------------------------------


class AuthorTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.author = Author.objects.create(
            first_name='John', last_name='Rowling', slug='john-rowling'
        )

    def test_author_creation(self):
        self.assertEqual(
            'John', self.author.first_name
        )
        self.assertEqual(
            'Rowling', self.author.last_name
        )
        self.assertEqual(
            'john-rowling', self.author.slug
        )


class TagTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.tag = Tag.objects.create(
            title='Test tag', slug='test-tag'
        )

    def test_author_creation(self):
        self.assertEqual(
            'Test tag', self.tag.title
        )
        self.assertEqual(
            'test-tag', self.tag.slug
        )


class PostTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.author = Author.objects.create(
            first_name='John', last_name='Rowling', slug='john-rowling'
        )
        tag1 = Tag.objects.create(
            title='Test tag 1', slug='test-tag-1'
        )
        tag2 = Tag.objects.create(
            title='Test tag 2', slug='test-tag-2'
        )
        cls.post = Post.objects.create(
            title='Test post', body='Test body', slug='test-slug',
            status='draft', author=cls.author
        )
        cls.post.tags.add(tag1, tag2)

        # creating votes to test get_rating method
        user_model = get_user_model()
        user1 = user_model.objects.create_user(
            username='Test',
            password='testpassword'
        )
        user2 = user_model.objects.create_user(
            username='Test2',
            password='testpassword2'
        )
        cls.vote1 = Vote.objects.create(
            user=user1, post=cls.post, value=1
        )
        cls.vote2 = Vote.objects.create(
            user=user2, post=cls.post, value=1
        )

    def test_post_creation(self):
        self.assertEqual(
            'Test post', self.post.title
        )
        self.assertEqual(
            'Test body', self.post.body
        )
        self.assertEqual(
            'test-slug', self.post.slug
        )
        self.assertEqual(
            'draft', self.post.status
        )
        self.assertEqual(
            self.author.pk, self.post.author_id
        )
        self.assertEqual(
            2, len(self.post.tags.all())
        )
        self.assertEqual(
            2, len(self.post.votes.all())
        )

    def test_get_rating_method(self):
        rating = self.post.get_rating()
        self.assertEqual(2, rating['value__sum'])


#  Views testing ------------------------------------------------------


class HomePageTests(TestCase):

    def setUp(self):
        self.response = self.client.get(reverse('blog:posts_list'))

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_template_used(self):
        self.assertTemplateUsed(self.response, 'blog/index.html')

    def test_homepage_view_resolves_home_url(self):
        view = resolve('/')
        self.assertEqual(
            view.func.__name__,
            PostListView.as_view().__name__
        )


class AuthorsListPageTests(TestCase):

    def setUp(self):
        self.response = self.client.get(reverse('blog:authors_list'))

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_template_used(self):
        self.assertTemplateUsed(self.response, 'blog/authors_list.html')

    def test_homepage_view_resolves_home_url(self):
        view = resolve('/authors/')
        self.assertEqual(
            view.func.__name__,
            AuthorListView.as_view().__name__
        )


class TagsListPageTests(TestCase):

    def setUp(self):
        self.response = self.client.get(reverse('blog:tags_list'))

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_template_used(self):
        self.assertTemplateUsed(self.response, 'blog/tags_list.html')

    def test_homepage_view_resolves_home_url(self):
        view = resolve('/tags/')
        self.assertEqual(
            view.func.__name__,
            TagListView.as_view().__name__
        )
