from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from .models import Post, Author, Tag


class PostListView(ListView):

    template_name = 'blog/index.html'
    context_object_name = 'posts'
    paginate_by = 10

    def get_queryset(self):
        qs = Post.objects.filter(status__exact='published')
        return qs


class PostDetailView(DetailView):

    template_name = 'blog/post_detail.html'

    def get_queryset(self):
        qs = Post.objects.filter(status__exact='published')
        return qs


class AuthorListView(ListView):

    model = Author
    template_name = 'blog/authors_list.html'


class TagListView(ListView):

    context_object_name = 'tags_list'
    model = Tag
    template_name = 'blog/tags_list.html'


class PostsByAuthor(ListView):

    template_name = 'blog/posts_by_author.html'
    context_object_name = 'posts'

    def get_queryset(self):
        author_slug = self.kwargs.get('slug')
        self.author = get_object_or_404(Author, slug=author_slug)
        qs = self.author.posts.filter(status__exact='published')
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['author'] = self.author
        return context


class PostsByTag(ListView):

    template_name = 'blog/posts_by_tag.html'
    context_object_name = 'posts'

    def get_queryset(self):
        tag_slug = self.kwargs.get('slug')
        self.tag = get_object_or_404(Tag, slug=tag_slug)
        qs = self.tag.posts.filter(status__exact='published')
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tag'] = self.tag
        return context
