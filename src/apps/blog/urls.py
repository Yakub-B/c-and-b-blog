from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path(
        '', views.PostListView.as_view(), name='posts_list'
    ),
    path(
        'authors/', views.AuthorListView.as_view(), name='authors_list'
    ),
    path(
        'tags/', views.TagListView.as_view(), name='tags_list'
    ),
    path(
        'post/<slug:slug>', views.PostDetailView.as_view(), name='post_detail'
    ),
    path(
        'author/<slug:slug>', views.PostsByAuthor.as_view(),
        name='posts_by_author'
    ),
    path(
        'tag/<slug:slug>', views.PostsByTag.as_view(), name='posts_bay_tag'
    ),

]
